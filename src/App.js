import './App.css';
import Navbar from './components/Navbar';
import Home from './pages/Home';  // Assuming your file is named Home.js
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';

function App() {
  return (
    <div className="App">
      <Router>
        <Navbar />
        <Routes>
          <Route path='/' element={<Home />} />
        </Routes>
        <h1>Test Text</h1>
        <h3>Be cool on another level!</h3>
      </Router>
    </div>
  );
}

export default App;
