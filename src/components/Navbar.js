import React from 'react';
import Logo from '../assets/tree_logo.png';
import { Link } from 'react-router-dom';
import '../styles/Navbar.css'; // Import the CSS file

function Navbar() {
  return (
    <div className='navbar'>
      <div className='leftPart'>
        <img src={Logo} alt='Logo' />
      </div>
      <div className='rightPart'>
        <Link to='/'>Home</Link>
        <Link to='/Leafs'>Leafs</Link>
        <Link to='/Sprout'>Sprout</Link>
        <Link to='/Login'>Login</Link> {/* Add the Login link */}
      </div>
    </div>
  );
}

export default Navbar;