## Node instillation and project creation



1. **Uninstall Node.js and npm:**

   If you have Node.js and npm installed globally, you should uninstall them first. Run the following commands in your terminal:

   ```bash
   # bash
   sudo npm uninstall -g create-react-app
   sudo npm uninstall -g npm
   sudo apt-get remove --purge nodejs
   sudo apt-get remove --purge npm
   ```
1. **Clean npm cache:**

   Sometimes issues can occur due to cached files. Clean the npm cache by running:

   ```bash
   # bash
   sudo npm cache clean -f
   ```
1. **Install Node Version Manager (NVM):**

   NVM is a tool that allows you to easily install and manage multiple  versions of Node.js on your machine. Install it with the following  commands:

   ```bash
   # bash
   curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.38.0/install.sh | bash
   source ~/.bashrc  # or restart your terminal
   ```
1. **Install Node.js 16 using NVM:**

   Now that you have NVM installed, you can use it to install Node.js 16:

   ```bash
   # bash
   nvm install 16
   nvm use 16
   ```
1. **Install create-react-app:**

   Finally, you can install `create-react-app` again using npm:

   ```bash
   # bash
   npm install -g create-react-app
   ```

6. **project creation:**

   ```bash
   nvm -v
   # if not the dowwnloaded verison:
   nvm install 16
   npx create-react-app <lower-case-name>
   ```

7. **project commds:**

   ```bash
     npm start
       #Starts the development server.
   
     npm run build
       #Bundles the app into static files for production.
   
     npm test
       #Starts the test runner.
   
     npm run eject
       #Removes this tool and copies build dependencies, configuration files
       #and scripts into the app directory. If you do this, you can’t go back!
   ```

   